/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flowas.datamodels.shop;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import lombok.Data;

/**
 *
 * @author liujh
 */
@Data
@Entity
public class ProductStock  extends IdAndDate{
    private boolean active;
    private String code;
    private String type;
    private String name;
    @Lob
    private String description;    
    private String address1;
    private String address2;
    private String addressAdditionalInformation;
    private String postalCode;
    private String city;
    private String stateCode;
    private String areaCode;
    private String countryCode;
    private String email;
    private String phone;
    private String fax;
    private String website;
    @ManyToOne
    private Retailer retailer;
    private String longitude;
    private String latitude;
    private Double distance;    
}
