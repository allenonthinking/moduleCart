package net.flowas.modulecart.rest;

import java.util.HashMap;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

public interface ConfigerationEndpoint {
	@GET
	@Path("ui.json")
	@Produces("application/json")
	HashMap listAll(@QueryParam("start") Integer startPosition, @QueryParam("max") Integer maxResult);
}